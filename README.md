#Android勉強会 ネットワーク通信 補足資料
##参考資料


[2.09. ネットワーク通信](https://github.com/mixi-inc/AndroidTraining/wiki/2.09.-%E3%83%8D%E3%83%83%E3%83%88%E3%83%AF%E3%83%BC%E3%82%AF%E9%80%9A%E4%BF%A1)

[Connecting to the Network](http://developer.android.com/training/basics/network-ops/connecting.html)

##概要
主にHTTP通信を行いそのコンテンツを取得する方法について説明します

###ソースコード
* livecoding ライブコーディングで作ったソースコード
* fundamntal 基本課題の回答ソースコード
* expert 応用課題の回答ソースコード

###流れ

ネットワークからコンテンツを取得してログに出力するソースコードをライブコーディング

→基本課題

→応用課題


##基本課題
* ボタンを押したらhttp://yahoo.co.jpからhtmlを取得し、TextViewに設定してください

###基本課題ヒント
* パーミッションを書く

<uses-permission android:name="android.permission.INTERNET" />

* AsyncTaskを使う
	型引数は<String,Void,String>　URL(文字列)を受け取り、HTML(文字列)を返すため
	doInBackgroundでネットワーク通信し、onPostExecuteでUIに設定

* ネットワーク通信は参考資料に

##応用課題
* ATND(アテンド)というWEBサービスのAPIでTextViewにイベント情報を一覧で表示してみましょう
	* 今回はAndroidから扱いやすいのでJSONとします。
	* 使用URL:http://api.atnd.org/events/?format=json

###応用課題ヒント
####JSONデータの確認
[JSON Parser Online](http://json.parser.online.fr/)にJSONの文字列を貼り付けると構造を見ることができます。
####JSONの扱い方
```
{"title":"JSON API!!!",
	"entries":[{"title":"entry one"},{"title":"entry two"}]}
```

このようなJSONの文字列を処理していく時の例を出します。

* JSONの文字列をパースしてJSONObjectを生成する



```
JSONObject jsonObject = new JSONObject(jsonString);
```

* JSONObjectの中にあるentries要素をJSONArrayとして取得する

```
JSONArray entriesJSONArray = jsonObject.getJSONArray("entries");
```

* JSONArrayからgetJSONObject(数字)でi番目の要素を取り出すと言った操作を行い表示する

```
JSONObject entry=entriesJSONArray.getJSONObject(0);
```

* その中のtitleを取得する

```
String title=entry.getString("title");
//これによってtitleにentry oneが入る
```



####JSONのパースとその利用

**ほぼ答えなので見たい人のみ見てください**

```
		TextView textView=(TextView)findViewById(R.id.textView1);
		try {
			//JSONの文字列をパースしてJSONObjectを生成する
			JSONObject jsonObject = new JSONObject(jsonString);
			//JSONObjectの中にあるevents要素をJSONArrayとして取得する
			JSONArray eventsJSONArray = jsonObject.getJSONArray("events");
			//JSONArrayのそれぞれに対して処理を行う
			for (int i = 0; i < eventsJSONArray.length(); i++) {
				//JSONArrayのi個目の要素を取得する
				JSONObject eventJSONObject = eventsJSONArray.getJSONObject(i);
				//eventJSONObjectのtitle要素を取得する
				String title = eventJSONObject.getString("title");
				//textViewに文字列を設定する
				textView.setText(textView.getText() + "\n-----\n" + title);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} 
```
