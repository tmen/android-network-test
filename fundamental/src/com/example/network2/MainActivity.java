package com.example.network2;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView textView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// テキストビューを取得してメンバ変数に入れる
		textView = (TextView) findViewById(R.id.textView2);
		// ボタンにクリックイベントを付ける
		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				contentsInit();
			}
		});
	}

	// ダウンロードしてその結果をTextViewにセットするメソッド
	private void contentsInit() {
		// AsyncTaskを無名クラス(無名オブジェクト)で作成
		new AsyncTask<String, Void, String>() {

			@Override
			protected String doInBackground(String... params) {
				return downloadUrl(params[0]);
			}

			// onPostExecuteでTextViewにセットする
			protected void onPostExecute(String result) {
				textView.setText(result);
			}
		}.execute("http://yahoo.co.jp");
	}

	// urlからその内容の文字列を取得するメソッドを書く
	private String downloadUrl(String urlString) {
		// 結果の文字列の組み立てるものを初期化して用意する
		StringBuilder stringBuilder = new StringBuilder();

		try { // IO Exceptionをcatchするためのtry

			InputStream inputStream = null;
			try { // finalyを使うためのtry

				// URLクラスのインスタンスの作成
				URL url = null;
				url = new URL(urlString);
				// URLからコネクションの作成
				URLConnection openConnection = null;
				openConnection = url.openConnection();
				openConnection.connect();

				// コネクションからinputstreamを取得する
				inputStream = openConnection.getInputStream();

				// inputstreamから文字列を取得していく
				while (true) {
					byte[] data = new byte[1024];
					int size = inputStream.read(data);
					if (size <= 0) {
						break;
					}
					stringBuilder.append(new String(data, "utf-8"));
				}
				// 処理を終了する
			} finally {
				if (inputStream != null) {
					inputStream.close();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// その文字列を返す
		return stringBuilder.toString();
	}
}
