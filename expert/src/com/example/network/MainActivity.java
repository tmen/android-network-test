package com.example.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity {
	TextView textView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textView = (TextView) findViewById(R.id.textView2);
		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showContents();
			}
		});
	}

	private void showContents() {
		AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
			@Override
			protected String doInBackground(String... params) {
				return downloadUrl(params[0]);
			}

			@Override
			protected void onPostExecute(String result) {
				showTextInJSONString(result);
			};
		};
		asyncTask.execute("http://api.atnd.org/events/?format=json");
	}

	private String downloadUrl(String urlString) {
		StringBuilder result = new StringBuilder();
		try {
			InputStream inputStream = null;
			try {
				URL url = new URL(urlString);
				URLConnection urlConnection = url.openConnection();
				urlConnection.connect();
				inputStream = urlConnection.getInputStream();
				while (true) {
					byte[] data = new byte[1024];
					int size = inputStream.read(data);
					if (size <= 0) {
						break;
					}
					result.append(new String(data, "utf-8"));
				}
			} finally {
				if (inputStream != null) {
					inputStream.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	private void showTextInJSONString(String jsonString) {
		
		try {
			//JSONの文字列をパースしてJSONObjectを生成する
			JSONObject jsonObject = new JSONObject(jsonString);
			//JSONObjectの中にあるevents要素をJSONArrayとして取得する
			JSONArray eventsJSONArray = jsonObject.getJSONArray("events");
			//JSONArrayのそれぞれに対して処理を行う
			for (int i = 0; i < eventsJSONArray.length(); i++) {
				//JSONArrayのi個目の要素を取得する
				JSONObject eventJSONObject = eventsJSONArray.getJSONObject(i);
				//eventJSONObjectのtitle要素を取得する
				String title = eventJSONObject.getString("title");
				//textViewに文字列を設定する
				textView.setText(textView.getText() + "\n-----\n" + title);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} 
	}
}
