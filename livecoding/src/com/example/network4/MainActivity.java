package com.example.network4;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new AsyncTask<String, Void, String>() {

					@Override
					protected String doInBackground(String... params) {
						return download(params[0]);
					}

					@Override
					protected void onPostExecute(String result) {
						super.onPostExecute(result);
						Log.d("TAG", result);
					}
				}.execute("http://yahoo.co.jp");

			}
		});
	}

	/**
	 * URLに対するHTMLを取得する
	 * @param urlString 取得したいコンテンツのURL
	 * @return HTMLなどのコンテンツ
	 */
	private String download(String urlString) {
		String result = "";
		InputStream inputStream = null;
		// IOExceptionをCatch
		try {
			try {
				// URLの文字列からURLオブジェクトを作成
				URL url = new URL(urlString);
				// URLオブジェクトからURLConnectionを取得
				URLConnection urlConnection = url.openConnection();
				// URLConnectionのconnectメソッド
				urlConnection.connect();
				// URLConnectionのInputStreamを取得します
				inputStream = urlConnection.getInputStream();
				// そのストリームからデータを取り出していく
				while (true) {
					byte[] data = new byte[1024];
					int size = inputStream.read(data);
					if (size <= 0) {
						break;
					}
					result = result + new String(data, "utf-8");
				}
				// 最後にInputStreamを閉じます
			} finally {
				if (inputStream != null) {
					inputStream.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
